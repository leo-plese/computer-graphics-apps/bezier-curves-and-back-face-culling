Bezier Curves and Back-face Culling. Implemented in Python using NumPy library and pyglet, Python OpenGL interface.

My lab assignment in Interactive Computer Graphics, FER, Zagreb.

Docs in "DokumentacijaIRGLabosi" under "6. VJEZBA."

Created: 2020
